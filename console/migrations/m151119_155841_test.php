<?php

use yii\db\Schema;
use yii\db\Migration;

class m151119_155841_test extends Migration
{
    public function up()
    {
         $sql="
--
-- Структура таблиці `authors`
--

CREATE TABLE IF NOT EXISTS `authors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп даних таблиці `authors`
--

INSERT INTO `authors` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Тестовий автор', 1425913115, 1425913115),
(2, 'Автор', 1425922579, 1425922579),
(3, 'Автор1', 1425922591, 1425922591);

-- --------------------------------------------------------

--
-- Структура таблиці `author_book`
--

CREATE TABLE IF NOT EXISTS `author_book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `book_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Дамп даних таблиці `author_book`
--

INSERT INTO `author_book` (`id`, `book_id`, `author_id`) VALUES
(2, 1, 1),
(5, 1, 2),
(6, 1, 3),
(7, 2, 3),
(8, 2, 1),
(9, 2, 2),
(11, 5, 2);

-- --------------------------------------------------------

--
-- Структура таблиці `books`
--

CREATE TABLE IF NOT EXISTS `books` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Дамп даних таблиці `books`
--

INSERT INTO `books` (`id`, `title`, `image`, `created_at`, `updated_at`) VALUES
(1, 'Тестовая', '0', 1425907985, 1425924537),
(2, 'Книга1', '0', 1425980665, 1425980665),
(3, 'qweqwe', '0', 1425996636, 1425996636),
(4, 'asdasd', '0', 1425996641, 1425996641),
(5, 'zxczxc', '0', 1425996647, 1425996647),
(6, 'qazqaz', '0', 1425996654, 1425996654),
(7, 'wsxwsx', 'Chrysanthemum.jpg', 1425996662, 1447943810);

-- --------------------------------------------------------

--
-- Структура таблиці `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `role` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Дамп даних таблиці `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `role`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'nt8YFA0lA_3u1-P6M_ZchYhLqFn2r5II', '$2y$13$LwB9MkhkfHrHdLqg7vBYjea55r23/UghBImYny.osHM5Oa5Xdyk.6', NULL, 'admin@mail.ru', 10, 20, 1424450325, 1425905057);

-- --------------------------------------------------------

--
-- Структура таблиці `user_book`
--

CREATE TABLE IF NOT EXISTS `user_book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `book_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп даних таблиці `user_book`
--

INSERT INTO `user_book` (`id`, `user_id`, `book_id`) VALUES
(1, 1, 1),
(2, 1, 2);
		";
        
		try {
            $this->execute($sql);
        } catch (Exception $e) {
			echo  $e->getMessage() . "\n";
		}
    }

    public function down()
    {
        echo "m151119_155841_test cannot be reverted.\n";

        return false;
    }
}
