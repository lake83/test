<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'statuses' => [10 => 'Активен', 0 => 'Отключен'],
    'roles' => [10 => 'Пользователь', 20 => 'Администратор']
];
