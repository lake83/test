<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_book".
 *
 * @property integer $id
 * @property integer $book_id
 * @property integer $user_id
 */
class UserBook extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_book';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'book_id'], 'required'],
            [['id', 'book_id', 'user_id'], 'integer']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'book_id' => 'Книга'
        ];
    }
    
    public function getBook()
    {
        return $this->hasOne(Books::className(), ['id' => 'book_id']);
    }
    
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}