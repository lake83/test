<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "books".
 *
 * @property integer $id
 * @property string $title
 * @property integer $created_at
 * @property integer $updated_at
 */
class Books extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'books';
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            [
                'class' => '\yiidreamteam\upload\ImageUploadBehavior',
                'attribute' => 'image',
                'thumbs' => ['thumb' => ['width' => 48, 'height' => 48]],
                'thumbPath' => '@frontend/web/images/thumbs/[[basename]]',
                'thumbUrl' => '/images/thumbs/[[basename]]',
                'filePath' => '@frontend/web/images/[[basename]]',
                'fileUrl' => '/images/[[basename]]'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpeg, jpg, gif'],
            [['title'], 'required'],
            [['title'], 'string', 'max' => 255],
            [['title'], function ($attribute) {
                                  $this->$attribute = \yii\helpers\HtmlPurifier::process($this->$attribute);}, 'on' => 'create,update']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'image' => 'Изображение',
            'created_at' => 'Создано',
            'updated_at' => 'Обновлено',
        ];
    }
}