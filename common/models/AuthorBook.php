<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "author_book".
 *
 * @property integer $id
 * @property integer $book_id
 * @property integer $author_id
 */
class AuthorBook extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'author_book';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['author_id'], 'required'],
            [['id', 'book_id', 'author_id'], 'integer']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'author_id' => 'Автор'
        ];
    }
    
    public function getAuthor()
    {
        return $this->hasOne(Authors::className(), ['id' => 'author_id']);
    }
}