<?php
/* @var $this yii\web\View */
$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Книги, находящихся на руках у читателей, и имеющих не менее трех со-авторов</h2>
                <ul>
                <?php
	                if(!empty($books))
                        foreach($books as $book)
                            echo '<li>'.$book['title'].'</li>';
                    else
                        echo 'Нет результатов';
                ?>
                </ul>
            </div>
            <div class="col-lg-4">
                <h2>Авторы, чьи книги в данный момент читает более трех читателей</h2>

                <?php
	                if(!empty($read_authors))
                        foreach($read_authors as $author)
                            echo '<li>'.$author['name'].'</li>';
                    else
                        echo 'Нет результатов';
                ?>
            </div>
            <div class="col-lg-4">
                <h2>Пять случайных книг</h2>

                <?php
	                if(!empty($rows))
                        foreach($rows as $row)
                            echo '<li>'.$row['title'].'</li>';
                    else
                        echo 'Нет результатов';
                ?>
            </div>
        </div>

    </div>
</div>
