<?php 
namespace app\controllers\actions;

use Yii;
use yii\web\NotFoundHttpException;

class Update extends \yii\base\Action
{
    public $model;
    public $success;
    
    public function run()
    {
        $model = $this->model;
        $model = $model::findOne(Yii::$app->request->getQueryParam('id'));
        
        if ($model == null) 
            throw new NotFoundHttpException('The requested page does not exist.');
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', $this->success);
            return $this->controller->redirect(['index']);
        } else {
            return $this->controller->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }
} 
?>