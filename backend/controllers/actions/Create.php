<?php 
namespace app\controllers\actions;

use Yii;

class Create extends \yii\base\Action
{
    public $model;
    public $scenario;
    public $success;
    
    public function run()
    {
        $model = empty($this->scenario) ? new $this->model : new $this->model(['scenario' => $this->scenario]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', $this->success);
            return $this->controller->redirect(['index']);
        } else {
            return $this->controller->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }
} 
?>