<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;

/**
 * UsedController implements the CRUD actions for UserBook model.
 */
class UsedController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    
    public function actions()
    {
        return [
            'index' => [
                'class' => 'app\controllers\actions\Index',
                'search' => 'backend\models\UserBookSearch'
            ],
            'create' => [
                'class' => 'app\controllers\actions\Create',
                'model' => 'common\models\UserBook',
                'success' => 'Запись о использовании добавлена.'
            ],
            'update' => [
                'class' => 'app\controllers\actions\Update',
                'model' => 'common\models\UserBook',
                'success' => 'Изменения сохранены.'
            ],
            'delete' => [
                'class' => 'app\controllers\actions\Delete',
                'model' => 'common\models\UserBook'
            ]
        ];
    }
}