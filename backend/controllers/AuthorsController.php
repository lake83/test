<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;

/**
 * AuthorsController implements the CRUD actions for Authors model.
 */
class AuthorsController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    
    public function actions()
    {
        return [
            'index' => [
                'class' => 'app\controllers\actions\Index',
                'search' => 'backend\models\AuthorsSearch'
            ],
            'create' => [
                'class' => 'app\controllers\actions\Create',
                'model' => 'common\models\Authors',
                'success' => 'Автор успешно добавлен.'
            ],
            'update' => [
                'class' => 'app\controllers\actions\Update',
                'model' => 'common\models\Authors',
                'success' => 'Изменения сохранены.'
            ],
            'delete' => [
                'class' => 'app\controllers\actions\Delete',
                'model' => 'common\models\Authors'
            ]
        ];
    }
}