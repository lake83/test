<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    
    public function actions()
    {
        return [
            'index' => [
                'class' => 'app\controllers\actions\Index',
                'search' => 'backend\models\UserSearch'
            ],
            'create' => [
                'class' => 'app\controllers\actions\Create',
                'model' => 'common\models\User',
                'scenario' => 'createUser',
                'success' => 'Пользователь успешно создан.'
            ],
            'update' => [
                'class' => 'app\controllers\actions\Update',
                'model' => 'common\models\User',
                'success' => 'Изменения сохранены.'
            ],
            'delete' => [
                'class' => 'app\controllers\actions\Delete',
                'model' => 'common\models\User'
            ]
        ];
    }
}
