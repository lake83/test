<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use backend\models\AuthorBookSearch;
use common\models\AuthorBook;

/**
 * BooksController implements the CRUD actions for Books model.
 */
class BooksController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    
    public function actions()
    {
        return [
            'index' => [
                'class' => 'app\controllers\actions\Index',
                'search' => 'backend\models\BooksSearch'
            ],
            'create' => [
                'class' => 'app\controllers\actions\Create',
                'model' => 'common\models\Books',
                'success' => 'Книга успешно добавлена.'
            ],
            'update' => [
                'class' => 'app\controllers\actions\Update',
                'model' => 'common\models\Books',
                'success' => 'Изменения сохранены.'
            ],
            'delete' => [
                'class' => 'app\controllers\actions\Delete',
                'model' => 'common\models\Books'
            ],
            'delAuthor' => [
                'class' => 'app\controllers\actions\Delete',
                'model' => 'common\models\AuthorBook'
            ],
        ];
    }
    
    public function actionAuthors($id)
    {
        $searchModel = new AuthorBookSearch();
        $searchModel->book_id = $id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        $author = new AuthorBook();
        
        if ($author->load(Yii::$app->request->post())) {
            $author->book_id = $id;
            if($author->save())
            {
                Yii::$app->session->setFlash('success', 'Автор добавлен.');
                return $this->redirect(['index']);
            }
        } else {
            return $this->renderAjax('authors', [
                'dataProvider' => $dataProvider,
                'author' => $author
            ]);
        }
    }
}