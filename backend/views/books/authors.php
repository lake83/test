<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use common\models\Authors;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Авторы книги';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить автора', '#', ['class' => 'btn btn-success', 'onclick' => 'js:$("#author-form").toggle();return false;']) ?>
    </p>
    
    <div id="author-form" style="display: none; margin-bottom: 30px;">
    <?php
	    $form = ActiveForm::begin();
        
        echo $form->field($author, 'author_id')->dropDownList(ArrayHelper::map(Authors::find()->all(), 'id', 'name'),['prompt'=>'- выбрать -']);
        
        echo Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
        
        ActiveForm::end();
    ?>
    </div>
    
    <?php Pjax::begin();
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'layout' => '{items}{pager}',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'author_id',
                'value' => 'author.name'
            ],

            [ 
                'class' => 'yii\grid\ActionColumn',
                'template' => '{delete}',
                'buttons' => [
                    'delete' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>',Url::to(['delAuthor', 'id' => $model->id]),
                            ['title' => 'Удалить']);
                    }
                ],
                'options' => ['width' => '60px']
            ]
        ],
    ]); 
    
    Pjax::end(); ?>

</div>
