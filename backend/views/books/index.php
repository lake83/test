<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Книги';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::button('Добавить книгу', ['value' => Url::to(['create']), 'class' => 'btn btn-success', 'id' => 'modalButton']) ?>
    </p>
    
    <?php
	   Modal::begin([
           'header' => '&nbsp;',
           'id' => 'modal',
       ]);
       echo '<div id="modalContent"></div>';
       
       Modal::end();
       
       $this->registerJs("$('#modalButton, .u_modal').click(function(){jQuery('#modal').modal('show').find('#modalContent').load($(this).attr('value'))});");
    ?>
    
    <?php Pjax::begin();
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout' => '{items}{pager}',
        'rowOptions'   => function ($model, $key, $index, $grid) {
            return ['data-id' => $model->id];},
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            [
                'attribute' => 'image',
                'format' => 'html',
                'value' => function ($model, $index, $widget) {
                    return '<img src="'.$model->getThumbFileUrl('image', 'thumb', '/images/anonymous.png').'"/>';}
            ],
            [
                'attribute' => 'created_at',
                'format' => 'date',
                'filter' => \yii\jui\DatePicker::widget([
                    'model'=>$searchModel,
                    'options' => ['class' => 'form-control'],               
                    'attribute'=>'created_at',
                    'language' => 'ru',
                    'dateFormat' => 'dd.MM.yyyy',
                ])
            ],
            [
                'attribute' => 'updated_at',
                'format' => 'date',
                'filter' => \yii\jui\DatePicker::widget([
                    'model'=>$searchModel,
                    'options' => ['class' => 'form-control'],               
                    'attribute'=>'updated_at',
                    'language' => 'ru',
                    'dateFormat' => 'dd.MM.yyyy',
                ])
            ],

            [ 
                'class' => 'yii\grid\ActionColumn',
                'template' => '{authors}{update}{delete}',
                'buttons' => [
                    'authors' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-list"></span>','#',
                            ['value' => Url::to(['books/authors', 'id' => $model->id]),
                             'class' => 'u_modal',
                             'title' => 'Авторы',
                             'onclick' => 'return false']);
                    },
                    'update' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>','#',
                            ['value' => $url,
                             'class' => 'u_modal',
                             'title' => 'Редактировать',
                             'onclick' => 'return false']);
                    }
                ],
                'options' => ['width' => '60px']
            ]
        ],
    ]); 
    
    Pjax::end();
    
    $this->registerJs("
    $('td').click(function (e) {
        var str = $(this).find('img').attr('src');
        if (str)
            jQuery('#modal').modal('show').find('#modalContent').html('<img width=\'565\' src=\'' + 
            ((str.indexOf('thumbs') + 1) ? str.replace(/\/thumbs/g, '') : str) + '\'/>');
    });"); ?>

</div>
