<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput() ?>
    
    <?= $form->field($model, 'email')->textInput() ?>
    
    <?php 
    if($model->isNewRecord)
        echo $form->field($model, 'password_hash')->passwordInput() ?>
    
    <?= $form->field($model, 'status')->dropDownList(Yii::$app->params['statuses']) ?>

    <?= $form->field($model, 'role')->dropDownList(Yii::$app->params['roles']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
