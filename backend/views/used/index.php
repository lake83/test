<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Используются';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::button('Добавить запись', ['value' => Url::to(['create']), 'class' => 'btn btn-success', 'id' => 'modalButton']) ?>
    </p>
    
    <?php
	   Modal::begin([
           'header' => '&nbsp;',
           'id' => 'modal',
       ]);
       echo '<div id="modalContent"></div>';
       
       Modal::end();
       
       $this->registerJs("$('#modalButton, .u_modal').click(function(){jQuery('#modal').modal('show').find('#modalContent').load($(this).attr('value'))});");
    ?>
    
    <?php Pjax::begin();
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout' => '{items}{pager}',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'user_id',
                'value' => 'user.username'
            ],
            [
                'attribute' => 'book_id',
                'value' => 'book.title'
            ],

            [ 
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}',
                'buttons' => [
                    'update' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>','#',
                            ['value' => $url,
                             'class' => 'u_modal',
                             'title' => 'Редактировать',
                             'onclick' => 'return false']);
                    }
                ],
                'options' => ['width' => '50px']
            ]
        ],
    ]); 
    
    Pjax::end(); ?>

</div>
